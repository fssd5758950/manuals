********************************
Editing Paths with the Node Tool
********************************

|Large icon for Node Tool| :kbd:`F2` or :kbd:`N`

The second-most used tool in Inkscape is the **Node tool**. It will be your friend when you need to **edit a path**.

.. admonition:: What you need to know about paths

   Every path consists of nodes that are connected to each other, like pearls on a string. The ends of this string can be connected like in a chain (closed path), or there can be two end nodes, that are only connected to one other node (open path).

   .. figure:: images/paths_open_and_closed.png
      :alt: Open and closed paths
      :class: screenshot

      On the left side, there is an open path. On the right side, it has been closed by connecting the two end nodes.

   Each node can only be connected to one or two other nodes. It's impossible to attach a third line to a node. When you need to draw a branching, you must create a separate path for one of the branches.

   In Inkscape, a node's position is marked by a square, circle or diamond
   handle on the line that represents the path.


A path's shape can be changed radically by moving the nodes it consists
of:

#. Activate the Node tool.
#. Click on the path to select it.
#. Click and drag the node you wish to reposition.

.. figure:: images/node_tool_node_with_adjacent_segments.png
   :alt: Node with incoming and an outgoing path segment.
   :class: screenshot

   This node has an incoming and an outgoing path segment connected to it.

.. figure:: images/node_tool_move_node.png
   :alt: Moving a node with the mouse.
   :class: screenshot

   You can move a node by grabbing it with the mouse.

For more gradual changes, paths can be edited by their nodes'
handles. Each node can have up to two circular handles that are connected to
it by thin lines. These handles can be moved around with the mouse, too. The handles control the shape of the adjacent path segment.

#. Activate the Node tool.
#. Click on the path to select it.
#. Click on the node you wish to edit.
#. Click and drag on the node's handle(s).

.. figure:: images/node_tool_shaping_segments.png
   :alt: Moving handles
   :class: screenshot

   You can modify path segments by moving the node's handles.

You can also manipulate the lines between the nodes (the path segments) directly,
using the Node tool. Click and drag on the path segment that connects two nodes,
to change its curvature, without having to use the handles for this.

.. figure:: images/node_tool_move_segment.png
   :alt: Moving segments
   :class: screenshot

   Dragging directly on the path gives quick results.

.. |Large icon for Node Tool| image:: images/icons/tool-node-editor.*
   :class: header-icon
